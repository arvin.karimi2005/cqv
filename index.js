'use strict';

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const app = express();
const config = require('./app/config/config');
const path = require('path');

//request validator for express
const expressValidator = require('express-validator');

const passport = require('passport');
app.use(passport.initialize());

// passport jwt authrization middlewar
const jwt_strategy = require('passport-jwt').Strategy;
const jwt_extract = require('passport-jwt').ExtractJwt;

app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());



function logErrors (err, req, res, next)
{
  console.error(err.stack)
  next(err)
}

function clientErrorHandler (err, req, res, next)
{
  if (req.xhr)
  {
    res.status(500).send({ error: 'Something failed!' })
  }
  else
  {
    next(err)
  }
}

function errorHandler (err, req, res, next)
{
  res.status(500).send({ error_name:err.name, error_code:err.code, error_message: err.hasOwnProperty('mapped')?err.mapped(): err.message })
  next()

}

console.log(`ENV = ${process.env.NODE_ENV}`);
if(process.env.NODE_ENV === "test")
{
  connect_mongo(config.test.db.uri, config.test.db.options)
	app.listen(config.test.server.port);
	console.log(`App listening on port ${config.test.server.port}`);
}
else
{
  connect_mongo(config.prod.db.uri, config.prod.db.options)
 	app.listen(config.prod.server.port);
 	console.log(`App listening on port ${config.prod.server.port}`);
}

// register strategy

// register routes
let paths = create_dir_path('v1');
register_strategy(paths.model_dir);
register_routes_v1(paths);


app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)

// function to connect mongo
function connect_mongo(uri, options)
{
  let db = mongoose.connect(config.test.db.uri,options);
  mongoose.Promise = global.Promise;

  //This callback will be triggered once the connection is successfully established to MongoDB
  mongoose.connection.on('connected', function ()
  {
    console.log(`Mongoose default connection open to ${uri}`);
  });
}

// create paths for api version
function create_dir_path(version)
{
  return {
    model_dir:path.join(__dirname,'app','models', version, '/'),
    route_dir:path.join(__dirname,'app','routes', version, '/'),
    controller_dir:path.join(__dirname,'app','controllers', version, '/')
  }
}


// registser auth strategy
function register_strategy(model_dir)
{
    let user_model = require(model_dir+'user.js');

    var opts =
    {
      jwtFromRequest: jwt_extract.fromHeader('authorization'),
      secretOrKey:'secret',
      ignoreExpiration: true
    }

    let strategy = new jwt_strategy(opts,function(jwt_payload, done)
    {
      user_model.findOne({_id:jwt_payload.user_id}, (err, user) =>
      {
        if (err) return done(null, false);
        else return done(null, user);
      });
    })

    passport.use(strategy);

}


// register routes
function register_routes_v1(paths)
{
  let router = express.Router({mergeParams: true});

  const user_route = require(path.join(paths.route_dir, 'user.js'))(router, paths.controller_dir, paths.model_dir);
  app.use('/v1', user_route);

  const lecture_route = require(path.join(paths.route_dir, 'lecture.js'))(router, paths.controller_dir, paths.model_dir);
  app.use('/v1', lecture_route);



  app.use(express.static(__dirname + '/public'));
  app.get('*', function(req, res)
  {
       res.sendFile(path.join(__dirname,'./public/Index.html')); // load the single view file (angular will handle the page changes on the front-end)
  });
}
