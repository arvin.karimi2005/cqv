var countSlide = 1;
var countSlideSugg = 1;
(function ($) {
    $(function () {
        var jcarousel = $('.jcarouselProduct .jcarousel');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 1100) {
                    width = width / 5;
                    countSlide = 5;
                } else if (width >= 800) {
                    width = width / 4;
                    countSlide = 4;
                }
                //else if (width >= 750) {
                //    width = width / 4;
                //    countSlide = 4;
                //}
                else if (width >= 600) {
                    width = width / 3;
                    countSlide = 3;
                } else if (width >= 200) {
                    width = width / 2;
                    countSlide = 2;
                } else if (width >= 100) {
                    width = width / 1;
                    countSlide = 1;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');

                $(".mostSaled .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.mostSaled .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".mostSaled .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.mostSaled .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".newest .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.newest .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".newest .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.newest .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".firstBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.firstBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".firstBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.firstBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".secondBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.secondBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".secondBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.secondBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".thirdBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.thirdBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".thirdBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.thirdBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".forthBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.forthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".forthBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.forthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".fifthBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.fifthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".fifthBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.fifthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".proBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.proBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });

                $(".proBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.proBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".catBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.catBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });

                $(".catBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.catBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".catProBoxList .jcarouselProduct .jcarousel-control-next").click(function () {
                    $('.catProBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });

                $(".catProBoxList .jcarouselProduct .jcarousel-control-prev").click(function () {
                    $('.catProBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });



                $(".mostSaled .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.mostSaled .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".mostSaled .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.mostSaled .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".newest .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.newest .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".newest .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.newest .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".firstBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.firstBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".firstBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.firstBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".secondBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.secondBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".secondBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.secondBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".thirdBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.thirdBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".thirdBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.thirdBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".forthBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.forthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".forthBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.forthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".fifthBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.fifthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".fifthBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.fifthBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".proBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.proBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".proBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.proBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".catBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.catBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".catBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.catBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });

                $(".catProBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.catProBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '+=' + countSlide);
                });
                $(".catProBoxList .jcarouselProduct .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.catProBoxList .jcarouselProduct .jcarousel').jcarousel('scroll', '-=' + countSlide);
                });


            })
            .jcarousel({
                wrap: 'circular'
            });

        var jcarouselSugg = $('.jcarouselSugg .jcarousel');
        jcarouselSugg
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();


                if (width >= 750) {
                    width = width / 3;
                    countSlideSugg = 3;
                } else if (width >= 600) {
                    width = width / 2;
                    countSlideSugg = 2;
                } else if (width >= 400) {
                    width = width / 1;
                    countSlideSugg = 1;
                }



                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');

                $(".suggestion .jcarouselSugg .jcarousel-control-next").click(function () {
                    $('.suggestion .jcarouselSugg .jcarousel').jcarousel('scroll', '+=' + countSlideSugg);
                });
                $(".suggestion .jcarouselSugg .jcarousel-control-prev").click(function () {
                    $('.suggestion .jcarouselSugg .jcarousel').jcarousel('scroll', '-=' + countSlideSugg);
                });

                $(".suggestion .jcarouselSugg .jcarousel .generalBoxHome ").on("swipeleft", function () {
                    $('.suggestion .jcarouselSugg .jcarousel').jcarousel('scroll', '+=' + countSlideSugg);
                });
                $(".suggestion .jcarouselSugg .jcarousel .generalBoxHome ").on("swiperight", function () {
                    $('.suggestion .jcarouselSugg .jcarousel').jcarousel('scroll', '-=' + countSlideSugg);
                });

            })
            .jcarousel({
                wrap: 'circular'
            });
    });
})(jQuery);
