$(function () {
    var jcarousel = $('.jcarouselGallery .jcarousel');

    jcarousel
        .on('jcarousel:reload jcarousel:create', function() {
            jcarousel.jcarousel('items').width(jcarousel.innerWidth());
        })
        .jcarousel({
            wrap: 'circular'
        });
        //.jcarouselAutoscroll({
        //    interval: 3000,
        //    target: '+=1',
        //    autostart: true
        //});

    $('.jcarouselGallery .jcarousel-control-prev')
            .on('jcarouselcontrol:active', function () {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function () {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });

    $('.jcarouselGallery .jcarousel-control-next')
            .on('jcarouselcontrol:active', function () {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function () {
                $(this).addClass('inactive');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselControl({
                target: '+=1'
            });
});
