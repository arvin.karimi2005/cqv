﻿app.run(function ($rootScope, $http, $location, $timeout, $interval, $window, md5, $routeParams,  $route, UserFactory) {
    $rootScope.pageName = "home";
    $rootScope.Config = ProjectConfig;
    $rootScope.tooltips = {};
    $rootScope.user = {};
    $rootScope.user.remember = false;
    $rootScope.forgotPassFlag = false;
    $rootScope.disappearSubscribeArea = false;
    $rootScope.bodyImg = "bodyImg4.jpg";
    $rootScope.preUrlName = "/";
    $rootScope.loginTxt = "ورود";
    $rootScope.bodyOverflow = "auto";
    $rootScope.inputsArea = true;
    $rootScope.txtWelcomeArea = false;
    $rootScope.subscribeLoading = false;
    $rootScope.successPart = false;
    $rootScope.errorPart = false;


    $rootScope.verticalMenuHeight = $(document).height();
    $rootScope.modalBodyHeight = $(window).height() - 200;

    //------------------------------------------------alerts

    $rootScope.hideAlert = function () {
        $rootScope.siteAlertDisplay = false;
        $rootScope.hideText = angular.element('.siteAlert strong');
        $rootScope.hideText.empty();
    };

    $rootScope.showAlert = function (color, txt) {
        $rootScope.hideAlert();
        if (color === "red") {
            $rootScope.alertClass = "alert-material-red";
        }
        else if (color === "green") {
            $rootScope.alertClass = "alert-material-green-200";
        }
        else if (color === "blue") {
            $rootScope.alertClass = "alert-material-light-blue-100";
        }
        else if (color === "orange") {
            $rootScope.alertClass = "alert-material-deep-orange-500";
        }
        $rootScope.alertTxt = txt.msg || JSON.stringify(txt.error_message);
        $rootScope.siteAlertDisplay = true;
        $rootScope.time = $timeout(function () {
            $rootScope.hideAlert();
        }, 5000);

        angular.element(".alert").append("<strong>" + $rootScope.alertTxt + "</strong>");

        $rootScope.topAlert = $(window).height() + window.pageYOffset - angular.element(document.querySelector('.siteAlert'))[0].offsetHeight - 75;
    };

    $rootScope.closeAlert = function () {
        $timeout.cancel($rootScope.time);
        $rootScope.hideAlert();
    };

    // $rootScope.Error = function (err)
    // {
    //     return err.msg;
    // }

    //------------------------------------------------login
    $rootScope.login = function () {
        //$rootScope.firstSignUp = false;
        $rootScope.fromLogin = true;
            UserFactory.Signin($rootScope.user.emailTxt, $rootScope.user.passTxt, $rootScope.user.remember).success(function (res) {
                if (res.token) {
                    let name = (res.name)?`${res.name.first} ${res.name.last}`:'undefiend';

                    window.localStorage.setItem("Token", res.token);
                    window.localStorage.setItem("Admin", res.admin);

                    window.localStorage.setItem("Name", name);

                        $rootScope.getLocalStorages();
                        $rootScope.closeModalSignInUp();

                }
            }).error(function (err) {
                $rootScope.showAlert("red", err);
            });
    };

    $rootScope.checkLogin = function () {
        if (window.localStorage.getItem("Token") !== null) {
            UserFactory.GetUserInfo(window.localStorage.getItem("Token")).success(function (res) {
                if (res) {
                    $rootScope.loginInfo = res;
                    //user._id
                    if ($rootScope.loginInfo.name !== undefined) {
                        window.localStorage.setItem("Name", $rootScope.loginInfo.name.first + " " + $rootScope.loginInfo.name.last);
                    }
                    $rootScope.getLocalStorages();
                    //if ($rootScope.fromLogin && ($rootScope.loginInfo.name !== undefined) && ($rootScope.loginInfo.name.first !== "" || $rootScope.loginInfo.name.first !== undefined)) {
                    //    $location.path("/");
                    //} else {
                    //    $location.path("/account");
                    //}
                } else {
                    $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
                }
            }).error(function (err, statusCode) {
                if (statusCode === 401) {
                    $rootScope.emptyLocalStorages();
                    $route.reload();
                }
                else
                    $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
            });
        }
    };

    $rootScope.logout = function () {
        $rootScope.emptyLocalStorages();
        $rootScope.getLocalStorages();
    };

    $rootScope.getPreUrl = function () {
        $rootScope.preUrlName = $location.url();
    };

    $rootScope.pressLogin = function (keyEvent) {
        if (keyEvent.which === 13) {
            $rootScope.login();
        }
    };

    $rootScope.register = function () {
        UserFactory.Signup($rootScope.user).success(function (res) {
            if (res) {
                $rootScope.firstSignUp = true;
                $timeout(function () {
                    $rootScope.login();
                }, 1000);
            } else {
                $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
            }
        }).error(function (err) {
            $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
        });
    };

    $rootScope.onclose = function () {
        if ($rootScope.firstSignUp) {
            $rootScope.inputsArea = true;
            $rootScope.txtWelcomeArea = false;
            $route.reload();
        }
    };

    //$rootScope.onExit = function () {
    //    alert('bye bye');
    //};

    //$window.onbeforeunload = $rootScope.onExit;

    //------------------------------------------------register from layout

    $rootScope.registerLayout = function (userL) {
        UserFactory.Signup(userL).success(function (res) {
            if (res) {
                $rootScope.firstSignUp = true;
                $rootScope.user.emailTxt = userL.emailTxt;
                $rootScope.user.passTxt = userL.passTxt;
                $timeout(function () {
                    $rootScope.login();
                }, 1000);
            } else {
                $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
            }
        }).error(function (err) {
            $rootScope.showAlert("red", "خطا! دوباره تلاش کنید.");
        });
    };

    //------------------------------------------------local storage

    $rootScope.getLocalStorages = function () {
        $rootScope.localStorageToken = window.localStorage.getItem("Token");
        $rootScope.localStorageName = window.localStorage.getItem("Name");
        $rootScope.localStorageAdmin = window.localStorage.getItem("Admin");
    };

    $rootScope.emptyLocalStorages = function () {
        window.localStorage.removeItem("Token");
        window.localStorage.removeItem("Name");
        window.localStorage.removeItem("Admin");
    };

    $rootScope.loggedin = function()
    {
        if (!$rootScope.localStorageToken)
        {
            $rootScope.showModalSignInUp('signin');
        }
        $rootScope.getLocalStorages();
        return $rootScope.localStorageToken;
    }


    $rootScope.readCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    //------------------------------------------------modal
    $rootScope.showModal = function () {
        $rootScope.inputsArea = true;
        $rootScope.modalShow = true;
        $rootScope.modalBGShow = true;
        $rootScope.modalBGHeight = screen.availHeight;
        //$rootScope.topPositionProduct = window.pageYOffset + (angular.element($window).outerHeight() / 3) - 100;
        $rootScope.topPositionProduct = 20;
    };

    $rootScope.closeModal = function () {
        $rootScope.modalEffect = "modalHideEffect";
        $timeout(function () { $rootScope.modalShow = false; }, 500);
        $rootScope.modalBGShow = false;
    };


    //------------------------------------------------modal for signin/signup

    $rootScope.showModalSignInUp = function (modalType) {
        $rootScope.signUpInTopDesc = "زندگی به سبک شما";
        $rootScope.checkModalType(modalType);
        $rootScope.showModal();
        $rootScope.modalSignInUp = true;
        $rootScope.modalSignInUpDisplay = "block";
        $rootScope.modalEffectSignInUp = "modalShowEffect";
        $timeout(function () {
            if ($(window).width() < 800) {
                $rootScope.bodyOverflow = "auto";
            } else {
                if ($(window).height() - 60 > $(".signInUpArea .modalArea").height()) {
                    $rootScope.bodyOverflow = "hidden";
                } else {
                    $rootScope.bodyOverflow = "auto";
                }
            }

        }, 100);

    };

    $rootScope.closeModalSignInUp = function () {
        //$route.reload();
        $rootScope.closeModal();
        $rootScope.modalEffectSignInUp = "modalHideEffect";
        $timeout(function () { $rootScope.modalSignInUp = false; $rootScope.modalSignInUpDisplay = "none"; }, 500);
        $rootScope.bodyOverflow = "auto";
    };

    $rootScope.checkModalType = function (modalType) {
        $timeout(function () {
            if ($(window).height() - 60 > $(".signInUpArea .modalArea").height()) {
                $rootScope.bodyOverflow = "hidden";
            } else {
                $rootScope.bodyOverflow = "auto";
            }
        }, 100);
        if (modalType === "signin") {
            $rootScope.signinModalType = true;
            $rootScope.signupModalType = false;
            $rootScope.loginTxt = "Login";
            $rootScope.forgotPassFlag = false;
        } else if (modalType === "signup") {
            $rootScope.signinModalType = false;
            $rootScope.signupModalType = true;
            $rootScope.forgotPassFlag = false;
        }
    };

    $rootScope.forgotPasswordClicked = function () {
        $rootScope.loginTxt = "ارسال";
        $rootScope.forgotPassFlag = true;
        $rootScope.forgotPassTxt = "لطفا ایمیل خود را وارد نمایید.";
    };

    $rootScope.backToNormalSignin = function () {
        $rootScope.loginTxt = "ورود";
        $rootScope.forgotPassFlag = false;
    };


    //------------	tooltips  ---------------

    $rootScope.tooltips = function () {
        $('[data-toggle=tooltip]').hover(function () {
            $(this).tooltip('show');
        }, function () {
            $(this).tooltip('hide');
        });
    };

    $timeout(function () { $rootScope.tooltips(); }, 500);
    //$rootScope.tooltips();

    //------------------------------------------------window resize data (subscribe)

    $rootScope.setSubscribeAreaHeight = function () {
        $(window).resize(function () {
            $rootScope.$apply(function () {
                $rootScope.subscribeImgAreaHeight = $(".subscribeImgArea").height();
            });
        });

        $timeout(function () {
            $rootScope.subscribeImgAreaHeight = $(".subscribeImgArea").height();
        }, 1000);
    };
    $rootScope.setSubscribeAreaHeight();

    //------------------------------------------------show subscribe

    $rootScope.showSubscribe = function () {
        $rootScope.showAfterDelay = "none";
        $timeout(function () {
            $rootScope.showAfterDelay = "block";
            $rootScope.setSubscribeAreaHeight();
        }, 500);
    };
    $rootScope.showSubscribe();

    //------------------------------------------------gallery

    $rootScope.scrollDownFromSlider = function () {
        var position = $("#scrollDownFromSlider").offset();
        $('html,body').animate({
            scrollTop: position.top - 50
        }, 500);
    };

    //------------------------------------------------compress menu

    $rootScope.manageCompressMenu = function () {
        //$rootScope.childMenuBox = 0;
        $rootScope.childMenuBoxWidth = "childMenuBoxNoWidth";
        $rootScope.childMenuBoxHeight = $(document).height();
        $rootScope.compressedMenuClicked = false;
        $rootScope.leftSide = "leftSlideOnLoad";
        $rootScope.disappearWithDelay = false;
    };

    $rootScope.manageCompressMenu();

    $rootScope.childMenuBoxClicked = function () {
        $rootScope.compressedMenuClicked = !$rootScope.compressedMenuClicked;
        if ($rootScope.compressedMenuClicked) {
            //$rootScope.childMenuBox = 1;
            $rootScope.disappearWithDelay = true;
            $rootScope.childMenuBoxWidth = "childMenuBoxAutoWidth";
            $rootScope.childMenuBoxHeight = $(document).height();
            $rootScope.leftSide = "leftSide";
            $rootScope.calcDocumentHeight();
            $timeout(function () {
                if ($(window).height() < $(".childMenu .childMenuBoxInsider").height()) $rootScope.childMenuBoxInsiderPosition = "absolute";
                else $rootScope.childMenuBoxInsiderPosition = "fixed";
            }, 500);
        } else {
            //$rootScope.childMenuBox = 0;
            $rootScope.childMenuBoxWidth = "childMenuBoxNoWidth";
            $rootScope.childMenuBoxHeight = $(document).height();
            $rootScope.leftSide = "backLeftSide";
            $timeout(function () {
                $rootScope.disappearWithDelay = false;
            }, 400);
        }
    };

    $rootScope.calcDocumentHeight = function () {
        $rootScope.documentHeight = $(document).height();
    }



    //------------------------------------------------on resize

    onResize = function () {
        if ($(window).width() <= 1140) {
            $(".menuArea a:nth-child(7)").css("display", "none");
            $(".menuArea a:nth-child(8)").css("display", "none");
        } else {
            $(".menuArea a:nth-child(7)").css("display", "inline-block");
            $(".menuArea a:nth-child(8)").css("display", "inline-block");
        }
    };

    $(document).ready(onResize);
    $(document).bind('load', onResize);
    $(window).bind('resize', onResize);

    //------------------------------------------------check persian or english character

    $rootScope.persianCharRange = /[\u0600-\u06FF]/;
    //alert($rootScope.persianCharRange.test(""));

    $rootScope.checkCharacterType = function (list) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].design) {
                if ($rootScope.persianCharRange.test(list[i].design.title)) {
                    list[i].titleDir = "dir-right";
                } else {
                    list[i].titleDir = "dir-left";
                }
            }

        }
    };

    //------------------------------------------------scrollTop Btn

    if (navigator.appVersion.toLowerCase().indexOf("mobile") !== -1) {
        $rootScope.scrollTopBtn = true;
    } else {
        $rootScope.scrollTopBtn = false;
    }

    window.onscroll = function () { $rootScope.scrollFunction() };

    $rootScope.scrollFunction = function () {
        if (document.body.scrollTop > 1500 || document.documentElement.scrollTop > 1500) $("#scrollTopBtn").css("display", "block");
        else $("#scrollTopBtn").css("display", "none");
    }

    $rootScope.topFunction = function () {
        $('html,body').animate({
            scrollTop: 0
        }, 500);
    }

    //------------------------------------------------mailchimp subscribe

    $rootScope.subscribeClicked = function () {
        $("#mce-responses .response").html("");
        $rootScope.subscribeLoading = false;
        $rootScope.successPart = false;
        $rootScope.errorPart = false;
        $rootScope.errorTxt = "";
        $rootScope.successTxt = "";

        if ($(".inputEmail").val() === "") {
            return;
        }
        $rootScope.subscribeLoading = true;
        $timeout(function () {
            var subEmail = $interval(function () {
                if ($("#mce-success-response").css("display") === "block") {
                    $rootScope.subscribeLoading = false;
                    $rootScope.successPart = true;
                    $rootScope.errorPart = false;
                    $rootScope.successTxt = "لطفا ایمیل خود را چک کنید";
                    if (!$rootScope.$$phase)
                        $rootScope.$apply();
                    $interval.cancel(subEmail);
                    return;
                } else if ($("#mce-error-response").css("display") === "block") {
                    $rootScope.subscribeLoading = false;
                    $rootScope.successPart = false;
                    if ($("#mce-error-response").html().indexOf("already subscribed") !== -1) {
                        $rootScope.errorTxt = "در حال حاضر عضو خبرنامه است.";
                        $rootScope.errorPart = false;
                    } else if ($("#mce-error-response").html().indexOf("signup requests") !== -1) {
                        $rootScope.errorTxt = "خطا: درخواست‌های مکرر!";
                        $rootScope.errorPart = false;
                    }
                    else {
                        $rootScope.errorTxt = "خطا";
                        $rootScope.errorPart = false;
                    }
                    if (!$rootScope.$$phase)
                        $rootScope.$apply();
                    $interval.cancel(subEmail);
                    return;
                } else {
                    $rootScope.subscribeLoading = false;
                    $rootScope.errorTxt = "خطا: ایمیل صحیح نیست!";
                    $rootScope.errorPart = false;
                    if (!$rootScope.$$phase)
                        $rootScope.$apply();
                    $interval.cancel(subEmail);
                }
            }, 500);
        }, 2000);
    };

    //------------------------------------------------initial

    $rootScope.scrollToTop = function () {
        //$location.hash('top');
        //$anchorScroll();
        window.scrollTo(0, 0);
    };

});
