app.controller('question_controller', function ($scope, $location, $timeout, $rootScope, $routeParams, $window, QuestionFactory) {
    $.material.init();

    $scope.new_question = {title:null, description:null}
    $scope.toggle = false
    $scope.show_create_modal = false
    $scope.lecture_id = $routeParams.lecture_id;

    $scope.get_question_list = function ()
    {

        QuestionFactory.get_questions($scope.lecture_id).success(function (res)
        {
            $scope.questions = res.question;
            $scope.lecture_title = res.title;
            $scope.lecture_created = res.created;
            $scope.speakers = res.speakers;
        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    };

    $scope.user_can_answer = function ()
    {
        let token = $rootScope.loggedin();

        QuestionFactory.user_can_answer(token, $scope.lecture_id).success(function (res)
        {
            $scope.can_answer = true;
        }).error(function (err)
        {
            $scope.can_answer = false;
        });
    };

    $scope.create_question = function ()
    {
        let token = $rootScope.loggedin();

        QuestionFactory.create_question(token, $scope.lecture_id, $scope.new_question, $scope.new_question).success(function (res)
        {
            $scope.get_question_list()
            $scope.close_modal();

        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    }

    $scope.vote = function (item)
    {
        let token = $rootScope.loggedin();

        QuestionFactory.vote(token, $scope.lecture_id, item._id).success(function (res)
        {
            $scope.get_question_list()
            $scope.close_modal();

        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    }

    $scope.answer = function (item)
    {
        let token = $rootScope.loggedin();

        QuestionFactory.answer(token, $scope.lecture_id, item._id, item.answer.content).success(function (res)
        {
            console.log(res);
            $scope.get_question_list()
            $scope.close_modal();

        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    }


    $scope.delete = function (item)
    {
        let token = $rootScope.loggedin();

        QuestionFactory.delete(token, $scope.lecture_id, item._id).success(function (res)
        {
            item.visible = false;
            $scope.close_modal();

        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    }

    $scope.show_model = function()
    {
        $scope.new_question = {title:null, description:null}
        $scope.show_create_modal = true;
        console.log(        $scope.show_create_modal);
    }

    $scope.close_modal = function()
    {
        $scope.show_create_modal = false
    }

    $scope.orderby = function(text)
    {
        $scope.toggle = !$scope.toggle
        $scope.order = text
    }

    $rootScope.getLocalStorages();
    $scope.is_admin = $rootScope.localStorageAdmin;
    $scope.get_question_list();
    $scope.user_can_answer();
});
