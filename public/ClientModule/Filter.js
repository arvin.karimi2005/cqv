﻿app.filter('availabaleProduct', function () {
    return function (status) {
        if (status)
            return "موجود";
        else
            return "ناموجود";
    };
});

app.filter('hiddenProduct', function () {
    return function (status) {
        if (status)
            return "نمایش بدهد";
        else
            return "نمایش ندهد";
    };
});

app.filter('yesOrNo', function () {
    return function (status) {
        if (status)
            return "بله";
        else
            return "خیر";
    };
});

app.filter('deliveryType', function () {
    return function (type) {
        if (type === "EMS")
            return "ارسال بسته";
        else if (type === "AG")
            return "تحویل در دفتر مرکزی";
        else if (type === "AG1")
            return "تحویل در دفتر شمال";
        else if (type === "-")
            return "-";
        else
            return "انتخاب کنید";
    };
});

app.filter('jalaliDate', function () {
    return function (inputDate, format) {
        if (inputDate === null || inputDate === "" || inputDate === undefined || inputDate.indexOf("-") > 0)
            return "-";
        var date = moment(inputDate);
        return date.format(format);
    };
});

app.filter('addressBook', function () {
    return function (item) {
        if (item)
            return item;
        else
            return "";
    };
});

app.filter('reverse', function () {
    return function (items) {
        if (items !== undefined && items !== null) {
            return items.slice().reverse();
        } else return null;
    };
});

app.filter('clothType', function () {
    return function (type) {
        if (type === "کتان")
            return "نخ پنبه اعلاء (۱۰۰٪ پنبه)";
        else if (type === "نایلون")
            return "پلی‌استر (۴۰٪ پنبه، ۶۰٪ اسپان پلی‌استر)";
        else
            return type;
    };
});