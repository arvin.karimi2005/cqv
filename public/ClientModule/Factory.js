﻿
app.factory('ListFactory', function ($http) {
    var ListFactory = {};

    ListFactory.get_lectures = function () {
        return $http({
            header: { 'Access-Control-Allow-Origin': '*' },
            url: ProjectConfig.URLServer + 'v1/lecture',
            method: "GET",
        });
    };

    ListFactory.join_speakers = function (token, lecture_id) {
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/join/'+lecture_id,
            method: "PATCH",
        });
    };


    ListFactory.create_lecture = function(token, title, description)
    {
        let req_data =
        {
            title:title,
            description:description
        }


        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture',
            method: "POST",
            data:req_data
        });
    }

    return ListFactory;
});

app.factory('QuestionFactory', function ($http) {
    var QuestionFactory = {};

    QuestionFactory.get_questions = function (lecture_id) {
        return $http({
            header: { 'Access-Control-Allow-Origin': '*' },
            url: ProjectConfig.URLServer + 'v1/lecture/question/'+lecture_id,
            method: "GET",
        });
    };

    QuestionFactory.user_can_answer = function (token, lecture_id) {
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/can_answer/'+lecture_id,
            method: "GET",
        });
    };

    QuestionFactory.vote = function (token, lecture_id, question_id) {
        console.log(question_id);
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/question/vote/'+lecture_id+'/'+question_id,
            method: "PATCH",
        });
    };
    //
    QuestionFactory.answer = function (token, lecture_id, question_id, content) {
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/question/answer/'+lecture_id+'/'+question_id,
            method: "PUT",
            data:{content:content}
        });
    };

    QuestionFactory.delete = function (token, lecture_id, question_id) {
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/question/'+lecture_id+'/'+question_id,
            method: "DELETE",
        });
    };


    QuestionFactory.create_question = function(token, lecture_id, question)
    {
        return $http({
            headers: { 'Authorization': token },
            url: ProjectConfig.URLServer + 'v1/lecture/question/'+lecture_id,
            method: "POST",
            data:question
        });
    }

    return QuestionFactory;
});



app.factory('UserFactory', function ($http) {
    var UserFactory = {};

    UserFactory.Signup = function (user) {
        var reqData = {
            username: user.emailTxt,
            password: user.passTxt,
            name: {
                first: user.firstTxt,
                last: user.lastTxt
            }
        }

        return $http({
            header: { 'Access-Control-Allow-Origin': '*' },
            url: ProjectConfig.URLServer + '/v1/user/signup',
            method: "POST",
            data: reqData
        });
    };

    UserFactory.Signin = function (email, pass, remember) {
        var reqData = {
            username: email,
            password: pass,
        }

        return $http({
            header: { 'Access-Control-Allow-Origin': '*' },
            url: ProjectConfig.URLServer + 'v1/user/login',
            method: "POST",
            data: reqData
        });
    };

    return UserFactory;
});
