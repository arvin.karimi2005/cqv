﻿var app = angular.module('VitaaminProject', ['angular-md5', 'ngRoute', 'ngSanitize', 'ngMdIcons', 'ui.bootstrap']);


app.config(function ($routeProvider, $locationProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'ClientView/lecture.html',
            controller: 'lecture_controller'
        }).when('/lecture/', {
            templateUrl: 'ClientView/lecture.html',
            controller: 'lecture_controller'
        }).when('/lecture/question/:lecture_id', {
            templateUrl: 'ClientView/question.html',
            controller: 'question_controller'
        })

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
});

$(document).ready(function () {
    $.material.init();
});
