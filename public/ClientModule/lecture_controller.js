﻿app.controller('lecture_controller', function ($scope, $location, $timeout, $rootScope, $routeParams, $window, ListFactory) {
    $.material.init();

    $scope.lectures = [];
    $scope.new_lecture = {title:null, description:null}
    $scope.show_create_modal = false


    $scope.get_lecture_list = function ()
    {
        ListFactory.get_lectures().success(function (res)
        {
            $scope.lectures = res;
        }).error(function (err)
        {
            $rootScope.showAlert("red", "err");
            $scope.showLoading = false;
        });
    };

    $scope.create_lecture = function ()
    {
        $scope.token = $rootScope.loggedin();

        ListFactory.create_lecture($scope.token, $scope.new_lecture.title, $scope.new_lecture.description).success(function (res)
        {
            $scope.lectures.push({_id:res._id, title:res.title, description:res.description});
            $scope.close_modal();

        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    }

    $scope.goto_question_clicked = function (item) {
        console.log(item);
        $location.path("/lecture/question/" + item._id);
    };

    $scope.join_speakers = function(item)
    {
        $scope.token = $rootScope.loggedin();

        ListFactory.join_speakers($scope.token, item._id).success(function (res)
        {
            $location.path("/lecture/question/" + item._id);
        }).error(function (err)
        {
            $rootScope.showAlert("red", err);
            $scope.showLoading = false;
        });
    };

    $scope.show_model = function()
    {
        $scope.new_lecture = {title:null, description:null}
        $scope.show_create_modal = true
    }

    $scope.close_modal = function()
    {
        $scope.show_create_modal = false
    }

    $rootScope.getLocalStorages();
    $scope.get_lecture_list();
});
