const passport = require('passport');
const { check,validationResult } = require('express-validator/check');


module.exports = function (router,  controller_dir, model_dir)
{

    const _lecture_controller = require(`${controller_dir}lecture.js`)(model_dir);

    router.get('/lecture',
    [
    ],
    (req, res, next)=>{_lecture_controller.get_lecture(req, res, next)});

    router.get('/lecture/:id',
    [
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.get_lecture(req, res, next)});

    router.post('/lecture',
    [
        check('title').isLength({min:3}).exists(),
        check('description').isLength({min:3}).exists(),
        (req, res, next)=> { validationResult(req).throw(); next()},
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.create_lecture(req, res, next)});

    router.patch('/lecture/join/:id',
    [
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.join_speakers(req, res, next)});


    router.get('/lecture/can_answer/:id',
    [
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.user_can_answer(req, res, next)});


    router.get('/lecture/question/:id',
    [
    ],
    (req, res, next)=>{_lecture_controller.get_all_questions(req, res, next)});

    router.post('/lecture/question/:id',
    [
        check('title').isLength({min:3}).exists(),
        check('description').isLength({min:3}).exists(),
        (req, res, next)=> { validationResult(req).throw(); next()},
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.create_question(req, res, next)});

    router.put('/lecture/question/answer/:id/:question_id',
    [
        check('content').isLength({min:3}).exists(),
        (req, res, next)=> { validationResult(req).throw(); next()},
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.answer_question(req, res, next)});

    router.patch('/lecture/question/vote/:id/:question_id',
    [
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.vote_question(req, res, next)});

    router.delete('/lecture/question/:id/:question_id',
    [
        passport.authenticate('jwt', { session: false })
    ],
    (req, res, next)=>{_lecture_controller.remove_question(req, res, next)});

    return router;
}
