const { check,validationResult } = require('express-validator/check');


module.exports = function (router, controller_dir, model_dir)
{

    const _user_controller_class = require(`${controller_dir}user.js`)
    const _user_controller = new _user_controller_class(model_dir);



    router.get('/user',_user_controller.get_users);

    router.post('/user/signup',
    [
        check('username').isLength({min:3}).exists(),
        check('password').isLength({min:3}).exists(),
        check('name.first').isLength({min:3}).optional(),
        check('name.last').isLength({min:3}).optional(),
        (req, res, next)=> { validationResult(req).throw(); next()}
    ],
    (req, res, next) => {_user_controller.signup(req, res, next)});

    router.post('/user/login',
    [
        check('username').isLength({min:3}).exists(),
        check('password').isLength({min:3}).exists(),
        (req, res, next)=> { validationResult(req).throw(); next()}
    ],
    (req, res, next)=>{_user_controller.login(req, res, next)});


    router.get('/user/admin',(req, res, next) => {_user_controller.setup_admin(req, res, next)});

    return router;
}
