const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

class user_controller
{
    constructor(model_dir)
    {
        this.models =
        {
            user:require(model_dir+'user.js')
        }

    }


    async get_users(req, res, next)
    {
        try
        {
            let err, users = await this.models.user.find().lean();
            res.send({"result":"welcome"})
        }
        catch (e)
        {
            next(e);
        }
    }

    async signup(req, res, next)
    {
        try
        {
            req.body.password = await bcrypt.hash(req.body.password, 10);
            let user = await this.models.user.create(req.body);
            res.json(user);
        }
        catch (e)
        {
            next(e);
        }
    };

    async login(req, res, next)
    {
        try
        {
            let user = await this.models.user.findOne({username:req.body.username}).lean().exec();
            let is_valid = (user)?await bcrypt.compare(req.body.password,user.password):false;

            if (user && is_valid)
            {
                let token = jwt.sign(
                {
                    user_id:user._id
                }, 'secret');

                res.json({token:token,admin:user.admin,name:user.name});
            }
            else
            {
                res.status(404).json({msg:"wrong creditionals"});
            }

        }
        catch (e)
        {
            next(e);
        }
    }

    async setup_admin(req, res, next)
    {
        try
        {
            let password = await bcrypt.hash("admin", 10);
            let user = await this.models.user.create({username:"admin","password":password ,admin:true});
            res.json({username:user.username, password:"admin"});

        }
        catch (e)
        {
            next(e);
        }
    }
}

module.exports = user_controller;
