const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

class lecture_controller
{
    constructor(model_dir)
    {
        this.models =
        {
            lecture:require(model_dir+'lecture.js')
        }
    }

    async get_lecture(req, res, next)
    {
        try
        {
            let lectures = await this.models.lecture.find().select({title:1, description:1,_id:1});
            res.json(lectures)
        }
        catch (e)
        {
            next(e);
        }
    }

    async create_lecture(req, res, next)
    {
        try
        {
            req.body['created']={by:req.user._id};

            let lecture = await this.models.lecture.create(req.body);
            res.status(201).json(lecture);
        }
        catch (e)
        {
            next(e);
        }
    }

    async join_speakers(req, res, next)
    {
        try
        {
            let lecture = await this.models.lecture.findByIdAndUpdate
            (
                req.params.id,
                {"$addToSet": {speakers: req.user._id}}
            );

            res.status(202).json({result:'ok'})
        }
        catch (e)
        {
            next(e)
        }
    }

    async get_all_questions(req, res, next)
    {
        try
        {
            let find = {_id:req.params.id, "question.visible":true};
            if (req.params.question_id) find['question._id']=req.params.question_id;

            let questions = await this.models.lecture.findOne(find)
            .populate({ path: 'created.by', select: 'username' })
            .populate({ path: 'question.created.by', select: 'username' })
            .populate({ path: 'question.answer.by', select: 'username' })
            .populate({ path: 'speakers', select: 'username' })
            res.json(questions)
        }
        catch (e)
        {
            next(e)
        }
    }

    async create_question(req, res, next)
    {
        try
        {
            let create =
            {
                title:req.body.title,
                description:req.body.description,
                created:
                {
                    by:req.user._id
                }
            }

            let question = await this.models.lecture.findByIdAndUpdate
            (
                req.params.id,
                {"$addToSet": {question: create}}
            );

            res.status(201).json(question);
        }
        catch (e)
        {
            next(e);
        }
    }

    async user_can_answer(req, res, next)
    {
        try
        {
            let lectures = await this.models.lecture.findById(req.params.id).select({speakers:true});
            let can_answer = lectures.speakers.findIndex((x)=>{return x.toString()==req.user._id.toString()});

            if (can_answer>-1)
            {
                res.status(200).json({"msg":"ok"})
            }
            else res.status(400).json({"msg":"cant access"})
        }
        catch (e)
        {
            next(e);
        }
    }

    async answer_question(req, res, next)
    {
        try
        {
            let lecture = await this.models.lecture.findOneAndUpdate
            (
                { _id:req.params.id,  "question._id": req.params.question_id},
                {
                    "$set":
                    {
                        "question.$.completed":true,
                        "question.$.answer":
                        {
                            content:req.body.content,
                            by:req.user._id,
                            at:new Date().toISOString(),
                        }
                    }
                }
            )

            if (lecture)
            {
                res.status(203).json({msg:"ok"});
            }
            else
            {
                res.status(403).json({msg:"cant find document or you dont have permision to answer this question"});
            }
        }
        catch (e)
        {
            next(e)
        }
    }

    async vote_question(req, res, next)
    {
        try
        {
            let lecture = await this.models.lecture.findOneAndUpdate
            (
                { _id:req.params.id, "question._id": req.params.question_id,"question.created.by._id":{"$ne":req.user._id}},
                {
                    "$addToSet":
                    {
                        "question.$.voters":req.user._id
                    }
                }
            )

            if (lecture)
            {
                res.status(203).json({msg:"ok"});
            }
            else
            {
                res.status(403).json({msg:"cant find document or you cant vote"});
            }
        }
        catch (e)
        {
            next(e)
        }
    }

    async remove_question(req, res, next)
    {
        try
        {
            if (req.user.admin == false)
            {
                return res.json({msg:"cant find document or you dont have permision to delete"});
            }

            let lecture = await this.models.lecture.findOneAndUpdate
            (
                { _id:req.params.id, "question._id": req.params.question_id},
                {
                    "$set":
                    {
                        "question.$.visible":false
                    }
                }
            )

            if (lecture)
            {
                res.status(204).json({msg:"ok"});
            }
        }
        catch (e)
        {
            next(e)
        }
    }

}

module.exports = (model_dir)=>{ return new lecture_controller(model_dir)};
