function question(question_model,controller)
{
    describe("Get all questions", function()
    {
         // Test will pass if we get all todos
        it("should return all questions", function(done)
        {
            var _question_mock = sinon.mock(question_model);
            var _expected_result = {status: true, questions: []};
            _question_mock.expects('find').yields(null, _expected_result);
            question_model.find(function (err, result) {
                _question_mock.verify();
                _question_mock.restore();
                expect(result.status).to.be.true;
                done();
            });
        });

        // Test will pass if we fail to get a todo
        it("should return error", function(done){
            var _question_mock = sinon.mock(question_model);
            var _expected_result = {status: false, error: "Something went wrong"};
            _question_mock.expects('find').yields(_expected_result, null);
            question_model.find(function (err, result) {
                _question_mock.verify();
                _question_mock.restore();
                expect(err.status).to.not.be.true;
                done();
            });
        });
    });
}

module.exports = question;
