const mongoose = require('mongoose');
const schema = mongoose.Schema;

let _lecture_schema = schema(
{
    title: {type: String, unique:true},
    description:{type: String},
    created:
    {
        at:{type: Date,default: Date.now},
        by:{type: schema.Types.ObjectId, ref:'user'}
    },
    question:
    [{
        title: {type: String},
        description:{type: String},
        completed:{type: Boolean, default: false},
        visible:{type: Boolean, default: true},
        created:
        {
            at:{type: Date,default: Date.now},
            by:{type: schema.Types.ObjectId, ref:'user'}
        },
        voters:[{type: schema.Types.ObjectId, ref:'user'}],
        answer:
        {
            content:{type: String},
            at:{type: Date,default: Date.now},
            by:{type: schema.Types.ObjectId, ref:'user'}
        }

    }],
    speakers:[{type: schema.Types.ObjectId, ref:'user'}]

});

_lecture_schema.virtual('votes').get(function()
{
    return this.voters.length;
});


//Exporting our model
var _lecture_model = mongoose.model('lecture', _lecture_schema);

module.exports = _lecture_model;
