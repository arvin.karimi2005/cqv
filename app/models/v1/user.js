const mongoose = require('mongoose');
const schema = mongoose.Schema;

let _user_schema = schema(
{
    username: {type: String, unique:true},
    password: {type: String},
    at:{type: Date,default: Date.now},
    name:
    {
        first:{type: String},
        last:{type: String}
    },
    admin:{type:Boolean, default:false}
});
//Exporting our model
var _user_model = mongoose.model('user', _user_schema);

module.exports = _user_model;
